import { Component, EventEmitter, Output } from '@angular/core';
import { DashboardService } from '../dashboard.service';
import { DashboardModel } from '../dashboardModel';


@Component({
  selector: 'app-user-management',
  templateUrl: './user-management.component.html',
  styleUrl: './user-management.component.css'
})
export class UserManagementComponent {

  form = new DashboardModel()

  @Output() afterSave = new EventEmitter<boolean>();

  constructor(
    private dashboardservice : DashboardService
  ){}

  insertData(){
    this.dashboardservice.insertData(this.form).subscribe(res=>{
      console.log(res)
      this.afterSave.emit();
    })
  }

}
