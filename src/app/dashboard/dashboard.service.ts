import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { DashboardModel } from './dashboardModel';


@Injectable({
  providedIn: 'root'
})
export class DashboardService {

  constructor(
    private httpClient: HttpClient
  ) { }

  httpOptions : any
  url = 'http://127.0.0.1:8000/api/bmi'

  getDataDetail(){
    return this.httpClient.get(this.url, this.httpOptions)
  }

  deleteData(data : DashboardModel){
    return this.httpClient.delete(this.url+'/'+data)
  }

  insertData(data : DashboardModel){
    return this.httpClient.post(this.url, data)
  }
}
