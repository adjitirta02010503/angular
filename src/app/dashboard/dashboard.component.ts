import { Component, inject } from '@angular/core';
import { Router } from '@angular/router';
import { DashboardService } from './dashboard.service';
import { DashboardModel } from './dashboardModel';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrl: './dashboard.component.css'
})
export class DashboardComponent {

  router = inject(Router)
  isDetailAdd = false;
  isDetailEdit = false;
  id : number = 0;
  resStringFy : string = 'Data Telah Terhhapus'
  model = new DashboardModel()

  columTable = ['NAMA','ALAMAT','UMUR','HOBBY','GENDER','TINGGI','BERAT','BERAT IDEAL','']

  dashboardDetail: any

  constructor(
    private dashboardservice : DashboardService
  ){ }

  ngOnInit(): void {
    this.getDataDetail()
    // this.userLogin()
  }

  getDataDetail(){
    this.dashboardservice.getDataDetail().subscribe(res => {
      this.dashboardDetail = res
      console.log(res)
    })
  }

  userLogin() {
    const user = localStorage.getItem('user')
    if(!user) {
      console.log('login');
      this.router.navigate(['/login']);
    }
  }

  deleteData(id : DashboardModel){
    this.dashboardservice.deleteData(id).subscribe(res => {
      this.resStringFy = JSON.stringify(res)
      this.getDataDetail()
    })
  }
}
