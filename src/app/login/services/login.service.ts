import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  private url = "https://dummyjson.com/auth/login"

  constructor(
    private http: HttpClient
  ) { }


  login(user: any){
    return this.http.post<any>(this.url, user)
  }
}
