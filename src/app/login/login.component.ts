import { Component, inject } from '@angular/core';
import { Router } from '@angular/router';
import { LoginService } from './services/login.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrl: './login.component.css'
})
export class LoginComponent {

  router = inject(Router)

  constructor(
    private auth: LoginService
  ){}
  form = {
    username: "",
    password: ""
  }

  login(){
    this.auth.login(this.form)
    .subscribe(res => {
      console.log(res);
      localStorage.setItem('user', JSON.stringify(res));
      this.router.navigate(['/dashboard']);
    })
  }

}
