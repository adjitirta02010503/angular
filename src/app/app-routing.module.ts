import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AboutComponent } from './about/about.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { UserManagementComponent } from './dashboard/user-management/user-management.component';
import { LoginComponent } from './login/login.component';
import { HomeComponent } from './master/home/home.component';
import { PagenotfoundComponent } from './master/pagenotfound/pagenotfound.component';

const routes: Routes = [
  {'path': '', component:HomeComponent},
  {'path': 'about', component:AboutComponent},
  {'path': 'login', component:LoginComponent},
  {'path': 'dashboard', component:DashboardComponent},
  {'path': 'useradd', component:UserManagementComponent},
  {'path': '**', pathMatch: 'full', component:PagenotfoundComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
